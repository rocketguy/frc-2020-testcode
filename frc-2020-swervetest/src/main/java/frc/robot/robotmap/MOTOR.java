package frc.robot.robotmap;

public enum MOTOR
{
    /**
     * Swerve pod drive motor IDs
     */
    FRONT_RIGHT_DRIVE(4),
    FRONT_LEFT_DRIVE(5),
    BACK_RIGHT_DRIVE(7),
    BACK_LEFT_DRIVE(6),

    /**
     * Swerve pod turn motor IDs
     */
    FRONT_RIGHT_TURN(13),
    FRONT_LEFT_TURN(1),
    BACK_RIGHT_TURN(3),
    BACK_LEFT_TURN(2);

    public int ID;

    private MOTOR(int id)
    {
        this.ID = id;
    }
}