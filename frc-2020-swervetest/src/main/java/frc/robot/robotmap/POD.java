package frc.robot.robotmap;

public enum POD
{
    FRONT_RIGHT,
    FRONT_LEFT,
    BACK_RIGHT,
    BACK_LEFT;
}