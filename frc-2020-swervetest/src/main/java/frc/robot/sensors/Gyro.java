package frc.robot.sensors;

import frc.robot.sensors.ADIS16448_IMU;
import frc.robot.utilities.Utils;

public class Gyro extends ADIS16448_IMU
{
    private double gyroBias = 0;

    /**
     * Initializes the gyro, calibrates it, and resets its values
     */
    public void init()
    {
        this.calibrate();
        this.reset();
        resetGyroBias();
    }

    public double getGyroZ()
    {
        return Utils.wrappedAngle(this.getAngleZ() - gyroBias);
    }

    public double getGyroZNoWrap()
    {
        return this.getAngleZ() - gyroBias;
    }

    public void resetGyroBias()
    {
        gyroBias = Utils.wrappedAngle(this.getAngleZ());
    }

}