/* 
 * Initially written by: Luke Newcomb
 * Last updated: Sep. 27, 2019
 */

package frc.robot.components;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.revrobotics.CANEncoder;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import com.revrobotics.ControlType;
import com.revrobotics.CANSparkMax.IdleMode;

import edu.wpi.first.wpilibj.MotorSafety;
import frc.robot.Constants;
import frc.robot.robotmap.MOTOR;
import frc.robot.robotmap.POD;
import frc.robot.utilities.PID;

/**
 * A component that represents each seperate swerve pod
 * We extend MotorSafety to ensure that if part of the robot is slow, then the motors
 * stop instead of running the robot into some(one)thing
 */
public class SwervePod extends MotorSafety
{

    private POD pod;

    private CANSparkMax driveMotor;
    private CANEncoder driveMotorEncoder;
    private CANPIDController driveMotorPID;

    private TalonSRX turnMotor;
    private PID turnMotorPID;

    private boolean sensorPhase = true;
    private boolean turnInvert = false;
    private boolean driveInvert = false;

    private final int NATIVE_IN_ROTATION = 24576;
    
    /**
     * SwervePod constructor
     * @param drive_motor (SparkMax) Motor enum based on the PDB number for the drive motor on SwervePod
     * @param turn_motor (TalonSRX) Motor enum based on the PDB number for the turn motor on SwervePod
     * @param motor EnumType for pod Name: FRONT_RIGHT, FRONT_LEFT, BACK_RIGHT, BACK_LEFT.
     */
    public SwervePod(MOTOR drive_motor, MOTOR turn_motor, POD pod)
    {
        /*
         * Initialize the drive motor
         */
        driveMotor = new CANSparkMax(drive_motor.ID, CANSparkMaxLowLevel.MotorType.kBrushless);
        driveMotorEncoder = driveMotor.getEncoder();
        driveMotorPID = driveMotor.getPIDController();

        driveMotor.setIdleMode(IdleMode.kCoast); // Set motor to coast

        /*
         * Initialize the turn motor
         */
        turnMotor = new TalonSRX(turn_motor.ID);
        turnMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, Constants.CONFIG_TIMEOUT_MS);
        turnMotor.setSensorPhase(sensorPhase);
        
        // turnMotor.configNeutralDeadband(0.001, Constants.kConfigTimeOutMs); ? 

		turnMotor.setNeutralMode(NeutralMode.Coast); // Set motor to coast
        turnMotor.setInverted(turnInvert);

		turnMotor.set(ControlMode.PercentOutput, 0.0); // Turns the turn motor to 0 power by default
		zeroTurnEncoder();

        this.pod = pod;

        this.setSafetyEnabled(true);
    }

    /**	
    * @param kP proportional constant for PID control
    * @param kI intergral constant for PID control
    * @param kD derivative constant for PID control
    * @param kIz I Zone is specified in the same units as sensor position
    * @param kFF feedforward constant for PID control
    * @param kMinOutput minimum PID output
    * @param kMaxOutput maximum PID output
    * @param deadBand PID deadband
    */
    public void configTurnPID(double kP, double kI, double kD, double kIz, double kFF, double kMinOutput, double kMaxOutput)
    {
        // Set PID coefficients for turn motor
		turnMotorPID = new PID(kP, kI, kD, kFF);
		turnMotorPID.setMaxIOutput(kIz);
		turnMotorPID.setOutputLimits(kMinOutput, kMaxOutput);
		turnMotorPID.setContinuous(true);
        turnMotorPID.setMaxInput(360);
        
        // Set the peak and nominal outputs, 1.0 is full
		turnMotor.configNominalOutputForward(0, Constants.CONFIG_TIMEOUT_MS);
		turnMotor.configNominalOutputReverse(0, Constants.CONFIG_TIMEOUT_MS);
		turnMotor.configPeakOutputForward(kMaxOutput, Constants.CONFIG_TIMEOUT_MS);
        turnMotor.configPeakOutputReverse(kMinOutput, Constants.CONFIG_TIMEOUT_MS);
        
		zeroTurnEncoder();
    }
    
    /**
     * Zeroes the turn motor encoder
     */
    public void zeroTurnEncoder()
    {
		turnMotor.setSelectedSensorPosition(0, 0, Constants.CONFIG_TIMEOUT_MS);
    }
    
    /**
     * Sets the turn motor inverted based on the value provided
     * @param value determines if the motor is inverted or not
     */
    public void setTurnInverted(boolean value)
    {
        turnInvert = value;
		turnMotor.setInverted(value);
    }

    /**
     * @param kP proportional constant for PID control
     * @param kI intergral constant for PID control
     * @param kD derivative constant for PID control
     * @param kIz I Zone is specified in the same units as sensor position
     * @param kFF feedforward constant for PID control
     * @param kMinOutput the minimum percentage to write to the output, -1 min
     * @param kMaxOutput the maximum percentage to write to the output, 1 max
     */
    public void configDrivePID(double kP, double kI, double kD, double kIz, double kFF, double kMinOutput, double kMaxOutput)
    {
        // set PID coefficients for turn motor
        driveMotorPID.setP(kP);
        driveMotorPID.setI(kI);
        driveMotorPID.setD(kD);
        driveMotorPID.setIZone(kIz);
        driveMotorPID.setFF(kFF);
        
        driveMotorPID.setOutputRange(kMinOutput, kMaxOutput);
        driveMotor.setInverted(driveInvert);

        // Set motor to coast
        driveMotor.setIdleMode(IdleMode.kCoast);
    }

    /**
     * Sets the drive motor inverted based on the value provided
     * @param value determines if the motor is inverted or not
     */
    public void setDriveInverted(boolean value)
    {
        driveInvert = value;
		driveMotor.setInverted(value);
    }

    /**
     * Reads the native (actual) encoder count
     * @return Encoder value in native units
     */
    public int getTurnNative()
    {
        return turnMotor.getSelectedSensorPosition(0);
    }

    /**
     * Reads the relative encoder angle in degrees
     */
    public double getTurnAngle()
    {
        // Convert rotations to degrees	   
        int nativeUnits = wrapNative(getTurnNative());
        double degrees = toAngle(nativeUnits);
        return degrees;
    }

    /**
     * Reads the absolute encoder angle in native units
     */
    public double getTurnAbsNative()
    {
		int absolutePosition = turnMotor.getSensorCollection().getPulseWidthPosition();

		/* Mask out overflows, keep bottom 12 bits */
        absolutePosition &= 0xFFF;
        
		return absolutePosition;
    }

    /**
     * Sets the drive motor speed
     * @param speed in RPM
     */
    public void setSpeed(double speed)
    {
        driveMotorPID.setReference(speed, ControlType.kVelocity);
        this.feed(); // Reset motor safety timeout
    }

    /**
     * Sets the new position of the turn motor through the PID controller
     * @param angle in degrees
     */
    public void setTurnMotorAngle(double angle)
    {
        turnMotor.set(ControlMode.PercentOutput, turnMotorPID.getOutput(getTurnAngle(), angle));
        this.feed(); // Reset motor safety timeout
    }
    
    /**
     * Turns the turn motor off, and resets the PID loop.
     */
    public void disableTurnMotor()
    {
		turnMotor.set(ControlMode.PercentOutput, 0.0);
		turnMotorPID.reset();
    }

    /**
	 * @param stallLimit The current limit in Amps at 0 RPM.
	 * @param freeLimit The current limit at free speed (5700RPM for NEO).
	 */
    public void setDriveCurrentLimit(int stallLimit, int freeLimit)
    {
		driveMotor.setSmartCurrentLimit(stallLimit, freeLimit);
    }
    
    /**
     * @param stallLimit The current limit in Amps at 0 RPM.
     * @param timeMsec The current limit at free speed (5700RPM for NEO).
     */
    public void setTurnCurrentLimit(int peakAmps, int durationMs, int continousAmps)
    {
        turnMotor.configPeakCurrentLimit(peakAmps, Constants.CONFIG_TIMEOUT_MS); // 15 A
        turnMotor.configPeakCurrentDuration(durationMs, Constants.CONFIG_TIMEOUT_MS); // 200ms
        turnMotor.configContinuousCurrentLimit(continousAmps, Constants.CONFIG_TIMEOUT_MS); // 10 A
        turnMotor.enableCurrentLimit(true);
    }

    /**
     * Returns the drive motor current draw
     * @return drive motor current draw
     */
    public double getDriveAmps()
    {
		return driveMotor.getOutputCurrent();
    }

    /**
     * Returns the turn motor current draw
     * @return turn motor current draw
     */
    public double getTurnAmps()
    {
		return turnMotor.getOutputCurrent();
    }
    
    /**
     * Returns the turn motor bus voltage
     * @return turn motor bus voltage
     */
    public double getTurnVoltage()
    {
		return turnMotor.getMotorOutputVoltage();
	}
    
    /**
     * Returns the drive motor bus voltage
     * @return drive motor bus voltage
     */
    public double getDriveVoltage()
    {
		return driveMotor.getBusVoltage();
	}
    
    /**
     * Gets the current speed the drive wheel is turning
     * @return ???
     */
    public double getSpeed()
    {
        // FIXME
		//Add the following constants to make proper speed calculations
		//(kMaxRPM  / 600) * (kSensorUnitsPerRotation / kGearRatio)
		double speed = driveMotorEncoder.getVelocity();
		speed = (speed/5.1)*(4*Math.PI)*(1/60.0)*(1/12.0);
		return speed;
	}

    /**
     * Gets the current position the drive motor is at in its revolution
     * @return ???
     */
    public double getPosition()
    {
        // FIXME
		double position = driveMotorEncoder.getPosition();
		position = (position/5.1)*(4*Math.PI);
		return position;
    }
    
    /**
     * Sets both motors in the pod to brake mode.
     */
    public void brakeOn()
    {
        driveMotor.setIdleMode(IdleMode.kBrake);
        disableTurnMotor();
        turnMotor.setNeutralMode(NeutralMode.Brake);
        this.feed(); // Reset motor safety timeout
	}
    
    /**
     * Unsets both motors in the pod to no longer breaking.
     */
    public void brakeOff()
    {
		driveMotor.setIdleMode(IdleMode.kCoast);
		driveMotorPID.setReference(0, ControlType.kDutyCycle);
        turnMotor.setNeutralMode(NeutralMode.Coast);
        this.feed(); // Reset motor safety timeout
	}
    
    /***
     * Gets the turn motor PID error
     * @return turn motor PID error
     ***/
    public double getTurnPIDError()
    {
		return turnMotorPID.getError();
	}
    
    /**
     * Wraps native units based on NATIVE_IN_ROTATION
     * @param units to be wrapped
     * @return Wrapped native units
     */
    private int wrapNative(int units)
    {
        int offset = units + NATIVE_IN_ROTATION;
        offset %= NATIVE_IN_ROTATION;
        return offset;
    }

    /**
     * Converts native units to an angle based on NATIVE_IN_ROTATION
     * @param units to be converted
     * @return Wrapped angle
     */
    private double toAngle(int units)
    {
		double angle = wrapNative(units); 
		angle *= 360.0 / NATIVE_IN_ROTATION;
		return angle;
    }
    
    /***
     * Converts angle to native units based on NATIVE_IN_ROTATION
     * @param angle to be converted
     * @return Native units
     ***/
    private int toNative(double angle)
    {
		return (int)((angle / 360) * NATIVE_IN_ROTATION);
	}

    /**
     * Stops the entire swerve pod, drive and turn motors.
     * Called by MotorSafety in the case that the motors time out
     */
    @Override
    public void stopMotor()
    {
		driveMotor.set(0.0);
        turnMotor.set(ControlMode.PercentOutput, 0.0);
        this.feed(); // Reset motor safety timeout
	}

    /**
     * Gives the motor description to the MotorSafety class to be printed in the RioLog if the motor times out
     */
    @Override
    public String getDescription()
    {
        return pod + " swerve pod";
    }
}