package frc.robot.utilities;

public class Utils
{
    /**
     * Converts an angle to an angle between 0 to 360
     * @param angle angle value
     * @return An angle between 0 and 360
     */
    public static double wrappedAngle(double angle)
    {
        angle = angle % 360; // Get the remainder of dividing by 360
        return (angle > 0) ? (angle) : (angle+360); // Returns a positive version of an angle
    }

    /**
     * Forces a value into a specific range
     * @param value input value
     * @param min maximum returned value
     * @param max minimum value in range
     * @return Value if it's within provided range, min or max otherwise
     */
    public static double constrain(double value, double min, double max)
    {
        if (value > max)
            return max;
        if (value < min)
            return min;
        return value;
    }

    /**
     * Test if the value is within the min and max, inclusive
     * @param value to test
     * @param min Minimum value of range
     * @param max Maximum value of range
     * @return true if value is within range, false otherwise
     */
    public static boolean bounded(double value, double min, double max)
    {
        // Note, this is an inclusive range. This is so tests like
        // `bounded(constrain(0,0,1),0,1)` will return false.
        // This is more helpful for determining edge-case behaviour
        // than <= is.
        return (min < value) && (value < max);
    }
}