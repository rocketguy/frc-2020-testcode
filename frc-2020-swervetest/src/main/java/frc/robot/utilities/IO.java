package frc.robot.utilities;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class IO
{
  public XboxController driver = new XboxController(0);

  Button toggleRobotDrive = new JoystickButton(driver, 1);

  /**
   * @return the horizontal axis value from the first driver controller
   */
  public double getDriverX()
  {
    return driver.getX();
  }

  /**
   * @return the vertical axis value from the first joystick on the driver controller
   */
  public double getDriverY()
  {
    return driver.getY();
  }

  /**
   * @return the horizontal axis value from the second joystick on the driver controller
   */
  public double getDriverZ()
  {
    return driver.getRawAxis(3);
  }

  /**
   * @return the vertical axis value from the second joystick on the driver controller
   */
  public double getDriverW() {
    return driver.getRawAxis(4);
  }


  /**
   * Reads the driver controller first joystick's horizontal value and applies an exponential function based on the exponent provided
   * @param exponent determines how steep the exponential function is
   */
  public double getDriverExpoX(double exponent)
  {
    return getExponential(getDriverX(), exponent);
  }

  /**
   * Reads the driver controller first joystick's vertical value and applies an exponential function based on the exponent provided
   * @param exponent determines how steep the exponential function is
   */
  public double getDriverExpoY(double exponent)
  {
    return getExponential(getDriverY(), exponent);
  }

  /**
   * Reads the driver controller second joystick's horizontal value and applies an exponential function based on the exponent provided
   * @param exponent determines how steep the exponential function is
   */
  public double getDriverExpoZ(double exponent)
  {
    return getExponential(getDriverZ(), exponent);
  }

  /**
   * Reads the driver controller second joystick's vertical value and applies an exponential function based on the exponent provided
   * @param exponent determines how steep the exponential function is
   */
  public double getDriverExpoW(double exponent)
  {
    return getExponential(getDriverW(), exponent);
  }

  /**
   * This function takes a joystick input and applies an exponential scaling
   */
  private double getExponential(double stickInput, double exponent)
  {
    double stickOutput;

    // stickOutput = e^(exponent*|stickInput|) - 1
    stickOutput = Math.exp(Math.abs(stickInput) * exponent) - 1; // Creates an exponential function starting at 0 and with a steepness based on the exponent
    stickOutput /= Math.exp(exponent) - 1; // Scales it back so that at input of 1.0, the output is 1.0
    stickOutput *= Math.signum(stickInput); // Reapplies polarity of the input

    return stickOutput;
  }

}
