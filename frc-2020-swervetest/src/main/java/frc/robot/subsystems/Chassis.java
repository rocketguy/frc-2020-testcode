/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.Constants;
import frc.robot.Robot;
import frc.robot.commands.Chassis.DriveWithJoysticks;
import frc.robot.components.SwervePod;
import frc.robot.robotmap.MOTOR;
import frc.robot.robotmap.POD;
import frc.robot.sensors.Gyro;
import frc.robot.utilities.Utils;


//This code use Ether's calculations to perform the inverse kinamatics equations for swerve RPM
//https://www.chiefdelphi.com/t/paper-4-wheel-independent-RPM-independent-steering-swerve/107383

/**
 * The subsystem in charge of controlling all of the serve pods to drive the robot.
 */
public class Chassis extends Subsystem
{

    public static Gyro gyro = new Gyro();

    private POD[] pods = {POD.FRONT_RIGHT, POD.FRONT_LEFT, POD.BACK_RIGHT, POD.BACK_LEFT};
    private SwervePod[] swervePods = new SwervePod[4];
    private double[] wheelAngles = new double[4];
    private double[] wheelSpeeds = new double[4];

    /**
     * Method that initializes the chassis and swerve pods
     */
    public void init()
    {
        // Declare all of the swerve pods
        SwervePod frontRightPod = new SwervePod(MOTOR.FRONT_RIGHT_DRIVE, MOTOR.FRONT_RIGHT_TURN, POD.FRONT_RIGHT);
        SwervePod frontLeftPod = new SwervePod(MOTOR.FRONT_LEFT_DRIVE, MOTOR.FRONT_LEFT_TURN, POD.FRONT_LEFT);
        SwervePod backRightPod = new SwervePod(MOTOR.BACK_RIGHT_DRIVE, MOTOR.BACK_RIGHT_TURN, POD.BACK_RIGHT);
        SwervePod backLeftPod = new SwervePod(MOTOR.BACK_LEFT_DRIVE, MOTOR.BACK_LEFT_TURN, POD.BACK_LEFT);
        
        swervePods[0] = frontRightPod;
        swervePods[1] = frontLeftPod;
        swervePods[2] = backRightPod;
        swervePods[3] = backLeftPod;

        // The left pods are meant to be inverted
        frontLeftPod.setDriveInverted(true);
        backLeftPod.setDriveInverted(true);
        frontRightPod.setDriveInverted(false);
        backRightPod.setDriveInverted(false);

        configDrivePID(Constants.P_DRIVE, Constants.I_DRIVE, Constants.P_DRIVE,
                       Constants.IZ_DRIVE, Constants.FF_DRIVE, Constants.MIN_OUTPUT_DRIVE, Constants.MAX_OUTPUT_DRIVE);
        configTurnPID(Constants.P_TURN, Constants.I_TURN, Constants.D_TURN,
                      Constants.IZ_TURN, Constants.FF_TURN, Constants.MIN_OUTPUT_TURN, Constants.MAX_OUTPUT_TURN);
        
        setDriveCurrentLimit(Constants.STALL_LIMIT_DRIVE, Constants.FREE_LIMIT_DRIVE);

        brakeOff();

        gyro.init();
    }

    /**
     * Runs the DriveWithJoysticks command when no other commands are being sent to the Chassis
     */
    @Override
    public void initDefaultCommand()
    {
        setDefaultCommand(new DriveWithJoysticks());
    }

    /**
     * Drives the robot based on driver controller input
     */
    public void drive()
    {
        double axisX = Utils.constrain(Robot.io.getDriverExpoX(1.5), 0.05, Constants.MAX_JOYSTICK_VALUE);
        double axisY = Utils.constrain(Robot.io.getDriverExpoY(1.5), 0.05, Constants.MAX_JOYSTICK_VALUE);

        double joyAngle = Math.atan2(-axisY, axisX);

        for(int i = 0; i < 4; i++)
        {
            // TODO Make actual drive code

            wheelAngles[i] = joyAngle;
            wheelSpeeds[i] = 0.5;

            swervePods[i].setTurnMotorAngle( Utils.wrappedAngle(wheelAngles[i]) );
            swervePods[i].setSpeed( wheelSpeeds[i] * Constants.MAX_RPM_DRIVE );
        }
    }

    /**
     * Stops all motors in every swerve pod
     */
    public void stop()
    {
        for(int i = 0; i < 4; i++)
            swervePods[i].stopMotor();
    }

    /**
     * Configures each swerve pod drive motor with the specified values
     * @param kP proportional constant for PID control
     * @param kI intergral constant for PID control
     * @param kD derivative constant for PID control
     * @param kIz I Zone is specified in the same units as sensor position
     * @param kFF feedforward constant for PID control
     * @param kMinOutput the minimum percentage to write to the output, -1 min
     * @param kMaxOutput the maximum percentage to write to the output, 1 max
     */
    public void configDrivePID(double kP, double kI, double kD, double kIz, double kFF, double kMinOutput, double kMaxOutput)
    {
        for(int i = 0; i < 4; i++)
        {
            swervePods[i].configDrivePID(kP, kI, kD, kIz, kFF, kMinOutput, kMaxOutput);
        }
    }

    /**
     * Configures each swerve pod turn motor with the specified values
     * @param kP proportional constant for PID control
     * @param kI intergral constant for PID control
     * @param kD derivative constant for PID control
     * @param kIz I Zone is specified in the same units as sensor position
     * @param kFF feedforward constant for PID control
     * @param kMinOutput minimum PID output
     * @param kMaxOutput maximum PID output
     * @param deadBand PID deadband
     */
    public void configTurnPID(double kP, double kI, double kD, double kIz, double kFF, double kMinOutput, double kMaxOutput)
    {
        for(int i = 0; i < 4; i++)
        {
            swervePods[i].configTurnPID(kP, kI, kD, kIz, kFF, kMinOutput, kMaxOutput);
        }
    }

    /**
     * Sets each swerve pod drive motor with the specified current limits
     * @param stallLimit
     * @param freeLimit
     */
    public void setDriveCurrentLimit(int stallLimit, int freeLimit)
    {
        for(int i = 0; i < 4; i++)
        {
            swervePods[i].setDriveCurrentLimit(stallLimit, freeLimit);
        }
    }

    /**
     * Sets each swerve pod turn motor with the specified current limits
     * @param peakAmps
     * @param durationMs
     * @param continuousAmps
     */
    public void setTurnCurrentLimit(int peakAmps, int durationMs, int continuousAmps)
    {
        for(int i = 0; i < 4; i++)
        {
            swervePods[i].setTurnCurrentLimit(peakAmps, durationMs, continuousAmps);
        }
    }

    /**
     * Sets each swerve pod to brake
     */
    public void brakeOn()
    {
        for(int i = 0; i < 4; i++)
        {
            if(swervePods[i] != null)
                swervePods[i].brakeOn();
        }
    }

    /**
     * Sets each swerve pod's brake to off
     */
    public void brakeOff()
    {
        for(int i = 0; i < 4; i++)
        {
            if(swervePods[i] != null)
                swervePods[i].brakeOff();
        }
    }

}
