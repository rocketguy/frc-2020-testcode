package frc.robot.robotmap;

public enum MOTOR
{
    /**
     * Swerve pod drive motor IDs
     */
    DRIVE(10),
    TURN(4);

    public int ID;

    private MOTOR(int id)
    {
        this.ID = id;
    }
}