/* 
 * Initially written by: Luke Newcomb
 * Last updated: Sep. 27, 2019
 */

package frc.robot.utilities;

import frc.robot.utilities.Utils;

public class PID
{
    private double P = 0.0, I = 0.0, D = 0.0, F = 0.0;

    private double maxIOutput = 1.0;
    private double maxError = 0.0;
    private double maxOutput = 0.0;
    private double minOutput = 0.0;

    private double error = 0.0;
    private double errorSum = 0.0;

    private double setpoint = 0.0;
    private double setpointRange = 0.0;
    
    private double outputRampRate = 0.0;
    private double outputFilter = 0.0;
    private double inputMax = 0.0;

    private double lastActual = 0.0;
    private double lastOutput = 0.0;

    private boolean firstRun = true;
    private boolean continuous = true;
    private boolean reversed = false;

    /***
     * Create a PID class object.
     * See setP, setI, setD methods for more details.
     * @param p Proportional gain.
     * @param i Integral gain.
     * @param d Derivative gain. (keep small...)
     ***/
    public PID(double p, double i, double d)
    {
        setPID(p, i, d);
    }

    /***
     * Create a PID class object.
     * See setP, setI, setD, setF methods for more details.
     * @param p Proportional gain.
     * @param i Integral gain.
     * @param d Derivative gain. (keep small...)
     * @param f Feed-forward gain. Open loop anticipation of what the output should be
     ***/
    public PID(double p, double i, double d, double f)
    {
        setPID(p, i, d, f);
    }

    /*************************
     * Operational functions *
     *************************/

    /**
     * Configure setpoint for the PID calculations
     * This represents the target for the PID system's, such as a
     * position, velocity, or angle.
     * @see PID getOutput(actual)
     * @param setpoint
     */
    public void setSetpoint(double setpoint)
    {
        this.setpoint = setpoint;
    }

    /**
     * Calculate the output value for the current PID cycle.<br>
     * @param actual The monitored value, typically as a sensor input.
     * @param setpoint The target value for the system
     * @return calculated output value for driving the system
     */
    public double getOutput(double actual, double setpoint)
    {
        double output;
        double Poutput;
        double Ioutput;
        double Doutput;
        double Foutput;

        setSetpoint(setpoint);

        // Ramp the setpoint used for calculations if user has opted to do so
        if (setpointRange != 0)
        {
            setpoint = Utils.constrain(setpoint, actual - setpointRange, actual + setpointRange);
        }

        // Do the simple parts of the calculations
        error = setpoint - actual;
        if (continuous)
        {
            // Wrap the error to [0, inputMax]
            error %= inputMax;

            // If going from 10 -> 350 around a circle, you must calculate the difference (-20)
            // If the error is larger than 180, then the real error is in the negative direction from 0
            if (Math.abs(error) > inputMax / 2)
            {
                if (error > 0) // If the input was 270
                    error -= inputMax; // 270 - 180 = 90, the real error
                else
                    error += inputMax; // -270 + 180 = -90, the real error
            }
        }

        // Calculate F output
        // Notice, this depends only on the setpoint, and not the error.
        Foutput = F * setpoint;

        // Calculate P term
        Poutput = P * error;

        // If this is our first time running this, we don't actually _have_ a previous input or output.
        // For sensor, sanely assume it was exactly where it is now.
        // For last output, we can assume it's the current time-independent outputs.
        if (firstRun)
        {
            lastActual = actual;
            lastOutput = Poutput+Foutput;
            firstRun = false;
        }

        // Calculate D Term
        // Note, this is negative. This actually "slows" the system if it's doing
        // the correct thing, and small values helps prevent output spikes and overshoot
        Doutput = -D * (actual - lastActual);
        lastActual = actual;

        // The Iterm is more complex. There's several things to factor in to make it easier to deal with.
        // 1. maxIoutput restricts the amount of output contributed by the Iterm.
        // 2. prevent windup by not increasing errorSum if we're already running against our max Ioutput
        // 3. prevent windup by not increasing errorSum if output is output=maxOutput
        Ioutput = I * errorSum;
        if (maxIOutput != 0)
            Ioutput = Utils.constrain(Ioutput, -maxIOutput, maxIOutput);

        // And, finally, we can just add the terms up
        output = Foutput + Poutput + Ioutput + Doutput;

        // Figure out what we're doing with the error.
        if (minOutput != maxOutput && !Utils.bounded(output, minOutput, maxOutput) )
        {
            // reset the error sum to a sane level
            // Setting to current error ensures a smooth transition when the P term
            // decreases enough for the I term to start acting upon the controller
            // From that point the I term will build up as would be expected
            errorSum=error;
        }
        else if (outputRampRate!=0 
             && !Utils.bounded(output, lastOutput - outputRampRate, lastOutput + outputRampRate) )
        {
            errorSum=error;
        }
        else if (maxIOutput!=0)
        {
            errorSum = Utils.constrain(errorSum + error, -maxError, maxError);
            // In addition to output limiting directly, we also want to prevent I term
            // buildup, so restrict the error directly
        }
        else
        {
            errorSum += error;
        }

        // Restrict output to our specified output and ramp limits
        if (outputRampRate != 0)
        {
            output = Utils.constrain(output, lastOutput - outputRampRate, lastOutput + outputRampRate);
        }
        if (minOutput != maxOutput)
        {
            output = Utils.constrain(output, minOutput, maxOutput);
        }
        if (outputFilter != 0){
            output = (lastOutput * outputFilter) + (output * (1 - outputFilter));
        }

        lastOutput = output; // Set the last output now that we are done
        return output;
    }

    /**
     * Calculate the output value for the current PID cycle.
     * In one parameter mode, the last configured setpoint will be used.
     * @see PID setSetpoint()
     * @param actual The monitored value, typically as a sensor input.
     * @param setpoint The target value for the system
     * @return calculated output value for driving the system
     */
    public double getOutput(double actual)
    {
        return getOutput(actual, setpoint);
    }

    /**
     * Calculate the output value for the current PID cycle.
     * In no-parameter mode, this uses the last sensor value,
     * and last setpoint value.
     * Not typically useful, and use of parameter modes is suggested.
     * @return calculated output value for driving the system
     */
    public double getOutput()
    {
        return getOutput(lastActual, setpoint);
    }

    /**
     * Resets the controller. This erases the I term buildup, and removes
     * D gain on the next loop.
     * This should be used any time the PID is disabled or inactive for extended
     * duration, and the controlled portion of the system may have changed due to
     * external forces.
     */
    public void reset()
    {
        firstRun = true;
        errorSum = 0;
    }

    /**
     * Set the maximum rate the output can increase per cycle.
     * This can prevent sharp jumps in output when changing setpoints or
     * enabling a PID system, which might cause stress on physical or electrical
     * systems.
     * Can be very useful for fast-reacting control loops, such as ones
     * with large P or D values and feed-forward systems.
     *
     * @param rate, with units being the same as the output
     */
    public void setOutputRampRate(double rate)
    {
        outputRampRate = rate;
    }

    /**
     * Set a limit on how far the setpoint can be from the current position
     * Can simplify tuning by helping tuning over a small range applies to a much larger range.
     * This limits the reactivity of P term, and restricts impact of large D term
     * during large setpoint adjustments. Increases lag and I term if range is too small.
     * @param range, with units being the same as the expected sensor range.
     */
    public void setSetpointRange(double range)
    {
        setpointRange = range;
    }

    /**
     * Set the continous range for the PID error. ensures that the error wraps from 0 -> inputRange
     * @param range, with units being the same as the expected sensor range.
     */
    public void setMaxInput(double max)
    {
        inputMax = max;
    }

    /**
     * Set the continuous range on for the PID error loop
     * @param value, boolean value for continuous mode
     */
    public void setContinuous(boolean value){
        continuous = value;
    }

    /**
     * Set a filter on the output to reduce sharp oscillations.
     * 0.1 is likely a sane starting value. Larger values use historical data
     * more heavily, with low values weigh newer data. 0 will disable, filtering, and use
     * only the most recent value.
     * Increasing the filter strength will P and D oscillations, but force larger I
     * values and increase I term overshoot.
     * Uses an exponential wieghted rolling sum filter, according to a simple
     * output*(1-strength)*sum(0..n){output*strength^n} algorithm.
     * @param output valid between [0..1), meaning [current output only.. historical output only)
     */
    public void setOutputFilter(double strength)
    {
        if(strength == 0 || Utils.bounded(strength, 0, 1))
            outputFilter = strength;
    }

    /**
     * Returns the current PID error
     * @return current PID error
     */
    public double getError()
    {
        return error;
    }

    /***********************
     * Getters and setters *
     ***********************/

    /***
     * Changes the P parameter. 
     *
     * @param p New gain value for the Proportional
     ***/
    public void setP(double p)
    {
        // Sets P, if it is not the right sign, then sets P = -p
        P = ((!reversed && p < 0) || (reversed && p > 0)) ? p : -p;
    }

    /***
     * Changes the I parameter
     * 
     * @param i New gain value for the Integral
     ***/
    public void setI(double i)
    {
        if(I != 0)
        {
            errorSum = errorSum * I / i;
        }
        if(maxIOutput != 0)
        {
            maxError = maxIOutput / i;
        }
        // Sets I, if it is not the right sign, then sets I = -i
        I = ((!reversed && i < 0) || (reversed && i > 0)) ? i : -i;
    }

    /***
     * Changes the D parameter 
     *
     * @param d New gain value for the Derivative
     ***/
    public void setD(double d)
    {
        // Sets D, if it is not the right sign, then sets D = -d
        D = ((!reversed && d < 0) || (reversed && d > 0)) ? d : -d;
    }

    /***
     * Changes the F parameter.
     * 
     * @param f New gain value for the Feed forward
     ***/
    public void setF(double f)
    {
        // Sets F, if it is not the right sign, then sets F = -f
        F = ((!reversed && f < 0) || (reversed && f > 0)) ? f : -f;
    }

    /***
     * Method overload for setPID(p,i,d,f);
     ***/
    public void setPID(double p, double i, double d)
    {
        setPID(p, i, d, F);
    }

    /***
     * See setP, setI, setD, setF methods for more detailed parameters.
     ***/
    public void setPID(double p, double i, double d, double f)
    {
        setP(P);
        setI(i);
        setD(d);
        setF(f);
    }

    /**
     * Set the maximum output for I component 
     */
    public void setMaxIOutput(double maximum)
    {
        maxIOutput=maximum;
        if(I != 0)
            maxError = maxIOutput / I;
    }

    /**
     * Maximum output range. 
     * @param output
     */
    public void setOutputRange(double output)
    {
        setOutputLimits(-output, output);
    }

    /**
     * Specify a maximum output.
     * When two inputs specified, output range is configured to
     * [minimum, maximum]
     * @param minimum possible output value
     * @param maximum possible output value
     */
    public void setOutputLimits(double minimum, double maximum)
    {
        if(maximum < minimum) return; // Wouldn't make any sense, so we don't change anything
        maxOutput = maximum;
        minOutput = minimum;

        // Ensure the bounds of the I term are within the bounds of the allowable output swing
        if(maxIOutput == 0 || maxIOutput > (maximum - minimum))
            setMaxIOutput(maximum-minimum);
    }

    /**
     * Set the operating direction of the PID controller
     * @param reversed Set true to reverse PID output
     */
    public void setDirection(boolean reversed)
    {
        this.reversed = reversed;
    }

}