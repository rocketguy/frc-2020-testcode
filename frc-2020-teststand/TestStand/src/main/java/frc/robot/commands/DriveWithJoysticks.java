package frc.robot.commands;

import frc.robot.Robot;
import edu.wpi.first.wpilibj.command.Command;

import frc.robot.utilities.Utils;
import frc.robot.Constants;
import frc.robot.utilities.IO;
import frc.robot.components.SwervePod;
import frc.robot.robotmap.MOTOR;

public class DriveWithJoysticks extends Command
{
    
    private double wheelSpeed;
    private double wheelAngle;
    private SwervePod swervePod;

    public DriveWithJoysticks()
    {

    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
        swervePod = new SwervePod(MOTOR.DRIVE, MOTOR.TURN);
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {	
        double axisX = Utils.constrain(Robot.io.getDriverExpoX(1.5), 0.05, Constants.MAX_JOYSTICK_VALUE);
        double axisY = Utils.constrain(Robot.io.getDriverExpoY(1.5), 0.05, Constants.MAX_JOYSTICK_VALUE);

        double joyAngle = Math.atan2(-axisY, axisX);

        wheelAngle = joyAngle;
        wheelSpeed = 0.5;

        swervePod.setTurnMotorAngle( Utils.wrappedAngle(wheelAngle) );
        swervePod.setSpeed( wheelSpeed * Constants.MAX_RPM_DRIVE );
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }

    // Called once after isFinished returns true
    protected void end()
    {
    	swervePod.stopMotor();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    	end();
    }
}
