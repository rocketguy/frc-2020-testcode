package frc.robot;

public class Constants
{
    public static final double P_DRIVE = 0.0005;
    public static final double D_DRIVE = 0.0;
    public static final double I_DRIVE = 0.0;
    public static final double IZ_DRIVE = 0.0;
    public static final double FF_DRIVE = 0.0001818;

    public static final double MAX_OUTPUT_DRIVE = 1.0;
    public static final double MIN_OUTPUT_DRIVE = -1.0;

    public static final double P_TURN = 0.005;
    public static final double D_TURN = 0.0001;
    public static final double I_TURN = 0.0;
    public static final double IZ_TURN = 0.0;
    public static final double FF_TURN = 0.0;

    public static final double MAX_OUTPUT_TURN = 1.0;
    public static final double MIN_OUTPUT_TURN = -1.0;

    public static final double MAX_RPM_DRIVE = 0.3;

    public static final double MAX_JOYSTICK_VALUE = 1.0;

    public static final int STALL_LIMIT_DRIVE = 40; // Amps
    public static final int FREE_LIMIT_DRIVE = 30; // Amps

    /*
     * When the configuration for a TalonSRX is updated, if a timeout
     * is configured, if configuration does not happen within the time,
     * an error will be thrown.
     */
    public static final int CONFIG_TIMEOUT_MS = 10;
}