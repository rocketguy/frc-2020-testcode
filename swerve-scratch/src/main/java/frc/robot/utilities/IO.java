/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.utilities;

import edu.wpi.first.wpilibj.XboxController;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class IO
{
    //// CREATING BUTTONS
    // One type of button is a joystick button which is any button on a
    //// joystick.
    // You create one by telling it which joystick it's on and which button
    // number it is.
    // Joystick stick = new Joystick(port);
    // Button button = new JoystickButton(stick, buttonNumber);

    // There are a few additional built in buttons you can use. Additionally,
    // by subclassing Button you can create custom triggers and bind those to
    // commands the same as any other Button.

    //// TRIGGERING COMMANDS WITH BUTTONS
    // Once you have a button, it's trivial to bind it to a button in one of
    // three ways:

    // Start the command when the button is pressed and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenPressed(new ExampleCommand());

    // Run the command while the button is being held down and interrupt it once
    // the button is released.
    // button.whileHeld(new ExampleCommand());

    // Start the command when the button is released and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenReleased(new ExampleCommand());

    private XboxController driver = new XboxController(0);

    public void init()
    {

    }

    /**
     * @return the horizontal axis value from the first driver controller
     */
    public double getDriverX()
    {
        return driver.getX();
    }

    /**
     * @return the vertical axis value from the first joystick on the driver controller
     */
    public double getDriverY()
    {
        return driver.getY();
    }

    /**
     * @return the horizontal axis value from the second joystick on the driver controller
     */
    public double getDriverZ()
    {
        return driver.getRawAxis(3);
    }

    /**
     * @return the vertical axis value from the second joystick on the driver controller
     */
    public double getDriverW() {
        return driver.getRawAxis(4);
    }

    /**
     * Reads the driver controller first joystick's horizontal value and applies an exponential function based on the exponent provided
     * @param exponent determines how steep the exponential function is
     */
    public double getDriverExpoX(double exponent)
    {
        return getExponential(getDriverX(), exponent);
    }

    /**
     * Reads the driver controller first joystick's vertical value and applies an exponential function based on the exponent provided
     * @param exponent determines how steep the exponential function is
     */
    public double getDriverExpoY(double exponent)
    {
        return getExponential(getDriverY(), exponent);
    }

    /**
     * Reads the driver controller second joystick's horizontal value and applies an exponential function based on the exponent provided
     * @param exponent determines how steep the exponential function is
     */
    public double getDriverExpoZ(double exponent)
    {
        return getExponential(getDriverZ(), exponent);
    }

    /**
     * Reads the driver controller second joystick's vertical value and applies an exponential function based on the exponent provided
     * @param exponent determines how steep the exponential function is
     */
    public double getDriverExpoW(double exponent)
    {
        return getExponential(getDriverW(), exponent);
    }

    /**
     * This function takes a joystick input and applies an exponential scaling
     */
    private double getExponential(double stickInput, double exponent)
    {
        double stickOutput;

        // stickOutput = e^(exponent*|stickInput|) - 1
        stickOutput = Math.exp(Math.abs(stickInput) * exponent) - 1; // Creates an exponential function starting at 0 and with a steepness based on the exponent
        stickOutput /= Math.exp(exponent) - 1; // Scales it back so that at input of 1.0, the output is 1.0
        stickOutput *= Math.signum(stickInput); // Reapplies polarity of the input

        return stickOutput;
    }
}
