package frc.robot;

public class Constants
{
    public static final double MAX_JOYSTICK_VALUE = 1.0;

    public static final int CONFIG_TIMEOUT_MS = 10;
}