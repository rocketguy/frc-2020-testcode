package frc.robot.robotmap;

public enum MOTOR
{
    /**
     * Swerve drive motor ID
     */
    DRIVE_MOTOR(4),

    /**
     * Swerve turn motor ID
     */
    TURN_MOTOR(6);

    public int ID;

    private MOTOR(int id)
    {
        this.ID = id;
    }
}