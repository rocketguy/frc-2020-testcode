/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.revrobotics.CANEncoder;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import com.revrobotics.ControlType;
import com.revrobotics.CANSparkMax.IdleMode;

import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.Constants;
import frc.robot.Robot;
import frc.robot.robotmap.MOTOR;
import frc.robot.utilities.Utils;

/**
 * A subsystem that takes care of the entire test stand
 */
public class SwerveStand extends Subsystem
{

    private CANSparkMax driveMotor;
    private CANEncoder driveMotorEncoder;
    private CANPIDController driveMotorPID;
    private TalonSRX turnMotor;

    private boolean sensorPhase = true;
    
    public void init()
    {
        driveMotor = new CANSparkMax(MOTOR.DRIVE_MOTOR.ID, CANSparkMaxLowLevel.MotorType.kBrushless);
        driveMotorEncoder = driveMotor.getEncoder();
        driveMotorPID = driveMotor.getPIDController();

        driveMotor.setIdleMode(IdleMode.kCoast);

        turnMotor = new TalonSRX(MOTOR.TURN_MOTOR.ID);
        turnMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, Constants.CONFIG_TIMEOUT_MS);
        turnMotor.setSensorPhase(sensorPhase);

        turnMotor.setNeutralMode(NeutralMode.Coast);

        turnMotor.set(ControlMode.PercentOutput, 0.0);
        turnMotor.setSelectedSensorPosition(0, 0, Constants.CONFIG_TIMEOUT_MS);
    }

    public void drive()
    {
        double axisZ = Utils.constrain(Robot.io.getDriverExpoZ(1.5), 0.05, Constants.MAX_JOYSTICK_VALUE);
        double axisW = Utils.constrain(Robot.io.getDriverExpoW(1.5), 0.05, Constants.MAX_JOYSTICK_VALUE);

        double joystickAngle = Math.atan2(-axisW, axisZ);
        double joystickMagnitude = Math.sqrt(axisW*axisW + axisZ*axisZ);

        driveMotorPID.setReference(joystickMagnitude * 0.3, ControlType.kVelocity);

        turnMotor.set(ControlMode.PercentOutput, joystickAngle / 10.0);
    }

    public void stop()
    {
		driveMotor.set(0.0);
        turnMotor.set(ControlMode.PercentOutput, 0.0);
    }

    @Override
    public void initDefaultCommand()
    {
    }
}
